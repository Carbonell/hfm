
rm(list=ls())

library(pheatmap)
source("model.R")


sim <- load_me("data/new_corrected_sim.RData")
sim$flow_fun <- solve_path_equations

# PREPARE DATA

xg <- sim$xg
wg <- sim$wg
hg <- sim$hg
xp <- sim$xp
wp <- sim$wp
hp <- sim$hp
xgp <- solve_path_equations(sim$xg, sim$equations)
wgp <- solve_path_equations(sim$wg, sim$equations)
kg <- sim$kg
kp <- sim$kp
s <- sim$s
equations <- sim$equations
derivs <- partial_derivatives(equations, rownames(xg))



res <- hmf(
  input=sim, derivs=derivs, max_it=1000, verbose=T,
  use_nnls=T, max_use_nnls=150, norm_h_by_sample=T, 
  alpha=5, beta=1, gamma=1, gamma2=1, phi=5, s_inference="optim", ro=3, r1=.1, r2=.0, r3=.1
)

plot_result(res)


